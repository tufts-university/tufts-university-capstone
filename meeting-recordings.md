## Meeting recording links

These are private since we talk about competitors and research data:
- Feb 3rd (Week 1): https://www.youtube.com/watch?v=ykRgoFRjNkE
- Feb 10th (Week 2): https://www.youtube.com/watch?v=fSCqu0D5A3U
- Feb 17th (week 3): https://www.youtube.com/watch?v=9uWwpSJnl8s
- Feb 22nd (Week 4): https://www.youtube.com/watch?v=qk9xgLPRcKA
- Mar 3rd (Week 5): No recording | [Meeting notes](https://docs.google.com/document/d/1vz6VfQZm6awRlu9p5GCZHxGbU_rbK1vMx1C_eFZJKyc/edit)
- Mar 10th (Week 6): https://www.youtube.com/watch?v=ZYQIpW6cJDc
- Mar 15th (Week 7): https://www.youtube.com/watch?v=sSl65fJNduw
- Mar 29th(Week 8): https://www.youtube.com/watch?v=92VtnbFksuw
- Apr 7th (Week 9): https://www.youtube.com/watch?v=92VtnbFksuw
- Apr 14th (Week 10): https://www.youtube.com/watch?v=IMk8PKmSzw0
- Apr 21st (Week 11): https://www.youtube.com/watch?v=VSok4yngHuY
- May 9th (Final presentation): https://www.youtube.com/watch?v=pZsAKgHuf2s
