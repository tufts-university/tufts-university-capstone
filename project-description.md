# Tufts University Capstone

## Details

The UX department will be sponsoring a Capstone course for [Tufts Human Factors Engineering/Engineering Pscyhology (BS) students](https://engineering.tufts.edu/me/current-students/undergraduate-program/bachelor-science-human-factors-engineering-bs). More information can be found in [this issue (private)](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2178). This project contains necessary data and information that students will need to create deliverables.

UX counterparts who are involved: `@gdoyle` `@leducmills` `@rayana`

## Project components

This project will be introduced on January 18th and culminate on May 6th in lieu of a recorded presentation, full report, and poster session that will be shared with the student class, instructors, and sponsors.

### Project definition

- Clear statement and definition of project goal
- Explanation of learning objectives for students and value back to GitLab
- The project calendar with GitLab team member and student team benchmarks/deliverables, assigned dates

#### Project write-up (what is shared with the students when they select a sponsor)

**Enable new GitLab developers to work faster**

Automation has become extremely necessary in today’s world, whether it be auto-payments for bills or even automatic car lights. What we don’t see everyday is the automation behind the scenes. Any enterprise platform depends on automation to push their code to their products. For example, if you were shopping on Amazon for something, but the button to “Add to cart” doesn't work, developers need to quickly fix that in the code and push it to production (the environment that you are buying the item from). [Continuous integration and deployment](https://docs.gitlab.com/ee/ci/) (CI/CD) can help them do that as quickly as possible, so you can get back to buying your items. [GitLab’s](https://about.gitlab.com/) CI/CD features are extremely important for developers, but with all the growth we’ve had, there are lots of improvements we can make to this experience. In this project, you’ll be using real customer feedback and analytics, as well as competitor products, to understand how the usability of pipelines can be improved so any new GitLab developer can get started quickly. We are looking to hear your recommendations, including research insights and mockups using our [Pajamas design system](https://design.gitlab.com/) (feel free to use our [Pajamas UI kit in Figma](https://www.figma.com/file/qEddyqCrI7kPSBjGmwkZzQ/Pajamas-UI-Kit)).

#### Learning objectives and value back to GitLab

**Students** will leave this Capstone with:
- An understanding of what GitLab CI/CD is and the importance of CI/CD in the industry world.
- Experience using real qualitative and quantitative data to make design recommendations and mockups/prototypes.
- (if time allows) Experience running solution validation with real developers.

**GitLab team members** will obtain:
- Mentorship and leadership experience.
- Exposure to GitLab as a product and our CI/CD features.
- Exposure to GitLab UX department.
- Experience sharing documented UX methods with others.

### Project deliverables (provided by Tufts students - due May 6th)

- `Required from GitLab` - Use real customer feedback and analytics, as well as competitor products, to understand how the usability of pipelines can be improved so any new GitLab developer can get started quickly. 
- `Required from GitLab` - Provide a set of recommendations, including research insights and mockups using our [Pajamas design system](https://design.gitlab.com/) (feel free to use our [Pajamas UI kit in Figma](https://www.figma.com/file/qEddyqCrI7kPSBjGmwkZzQ/Pajamas-UI-Kit)).
- `Required from Tufts instructors` - Final project video, poster, and presentation.

### Project deliverables (provided by GitLab team members)

Deliverables need to follow the NDA set forth by GitLab and [GitLab's SAFE framework](https://about.gitlab.com/handbook/legal/safe-framework/#safe). All deliverables are listed as issues with the ~"Student Deliverable" label in the [epic](https://gitlab.com/groups/tufts-university/-/epics/1) and include a To-Do List to ensure that they are SAFE before sharing publicly. 

Research that the students will use in their discovery phase includes [SUS verbatim](https://gitlab.com/tufts-university/tufts-university-capstone/-/issues/3) and [dashboard screenshots](https://gitlab.com/tufts-university/tufts-university-capstone/-/issues/2). They will also be able to use the configured pipeline for this project along with learning resources in https://gitlab.com/tufts-university/tufts-university-capstone/-/blob/main/resources.md. All research reports will need to follow [GitLab's SAFE framework](https://about.gitlab.com/handbook/legal/safe-framework/#safe).

Final deliverables will be owned by GitLab.
