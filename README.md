## 👋 Welcome!

This project contains all of the progress and deliverables that the Tufts University students made on their Capstone Project that was sponsored by GitLab. Please check out the [project description file](https://gitlab.com/tufts-university/tufts-university-capstone/-/blob/main/project-description.md) for more details.

## Findings

See https://gitlab.com/gitlab-org/ux-research/-/issues/2500 for the summary of the students' findings.
