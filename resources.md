## 📚 Learn about CI/CD

To help you get a better understanding of what CI/CD is at GitLab, you can look into these resources:
- [Tutorial: Create and run your first GitLab CI/CD pipeline](https://docs.gitlab.com/ee/ci/quick_start/)
- [CI/CD concepts](https://docs.gitlab.com/ee/ci/introduction/)
- [Pipelines: a book of everything](https://docs.gitlab.com/ee/ci/pipelines/)

## Example project

We've added an example of a working pipeline to this project so you can play around with the features yourself. 

### ✏️ Pipeline configuration explained

The `.gitlab-ci.yml` file is what controls and configures the pipelines that run in this project.
  👉 [See what this looks like ](https://gitlab.com/tufts-university/tufts-university-capstone/-/ci/editor?branch_name=main)

Our pipeline configuration has 3 stages:
- `build`, `test`, and `deploy`
👉 [See what this looks like in lines 19-22](https://gitlab.com/tufts-university/tufts-university-capstone/-/ci/editor?branch_name=main)

Each stage can have one or more jobs. You can think of the stage as a bucket 🪣 and the jobs as items 🎾 🧩 in that bucket. Our pipeline configurations has:

**1 job in the `build` stage.** 

The `build-job` uses the `echo` command which spits out a string of text that you provide it ([more info](https://hbctraining.github.io/Intro-to-shell-flipped/lessons/05_shell-scripts_variable.html#:~:text=The%20echo%20command%20is%20used,screen%20or%20to%20a%20file.)). All we are doing here is displaying 2 lines of code that are defined in [lines 28 & 29](https://gitlab.com/tufts-university/tufts-university-capstone/-/ci/editor?branch_name=main).

**2 jobs in the `test` stage.**

The `unit-test-job` job prints out a string defined in [line 34](https://gitlab.com/tufts-university/tufts-university-capstone/-/ci/editor?branch_name=main), then "sleeps" for 60 seconds ([more info](https://www.freecodecamp.org/news/bash-sleep-how-to-make-a-shell-script-wait-n-seconds-example-command/)), and finally prints out another string defined in [line 36](https://gitlab.com/tufts-university/tufts-university-capstone/-/ci/editor?branch_name=main). The `lint-test-job` job does the same as the unit-test-job, but prints different strings and sleeps for a shorter amount of time. 

**1 job in the `deploy` stage.**

The `deploy-job` defines an environment to push the code to in [line 48](https://gitlab.com/tufts-university/tufts-university-capstone/-/ci/editor?branch_name=main), named `production`. This will tell GitLab to create an [environment with deployments](https://docs.gitlab.com/ee/ci/environments/) each time the `deploy-job` is run. The job also uses the `echo` command to print out some strings.

### 🕹️ Pipelines and jobs

You can run the pipeline configuration by using the Run pipeline button on [the pipelines page](https://gitlab.com/tufts-university/tufts-university-capstone/-/pipelines). The pipelines page also will show a list of all pipelines run for this project.
👉 [Successful pipeline example](https://gitlab.com/tufts-university/tufts-university-capstone/-/pipelines/758499556)
👉 [Failed pipeline example](https://gitlab.com/tufts-university/tufts-university-capstone/-/pipelines/758512976)

Each pipeline details page includes a visualization of what that pipeline looks like and the jobs within it. If you want to see all of the jobs run in this project, you can go to the [jobs page](https://gitlab.com/tufts-university/tufts-university-capstone/-/jobs).

### 🚀 Environments and deployments

Since we define a `production` environment to deploy the code in the `deploy-job` job to, you'll see a production environment on the [environments page](https://gitlab.com/tufts-university/tufts-university-capstone/-/environments). Each time we run the pipeline, a new deployment is added to the production environment. You can see this in the [environments details page](https://gitlab.com/tufts-university/tufts-university-capstone/-/environments/13136372). If we were creating a website with this project, you would be able to visit the URL of the production environment through the environments page as well.

👉 [Viewable environment examples](https://gitlab.com/gitlab-org/gitlab/-/environments)

## Use real data to help us improve the pipeline experience

We have a combination of qualitative and quantitative data that was collected through various research studies and is tracked regularly from gitlab.com and self-managed instances (if you were to install GitLab locally rather than using the website).

👉 [Qualitative data](https://gitlab.com/tufts-university/tufts-university-capstone/-/issues/3)
👉 [Quantitative data](https://gitlab.com/tufts-university/tufts-university-capstone/-/issues/2)

## Questions? Want us to review your work?

Reach out to us in our [shared Slack channel #tufts_capstone](https://gitlab.slack.com/archives/C04K87V8V0V) (**primary**) or email!

**Gina (Product Designer: Runner & Pipeline Insights)**

💬 Slack handle: `@gdoyle`

📧 Email: `gdoyle@gitlab.com`

✨ GitLab profile page: https://gitlab.com/gdoyle

**Rayana (Product Design Manager: Verify, Package, & Release)**

💬 Slack handle: `@Rayana`

📧 Email: `rverissimo@gitlab.com`

✨ GitLab profile page: https://gitlab.com/rayana

**Ben (Senior UX Researcher: Create)**

💬 Slack handle: `@@Ben Leduc-Mills`

📧 Email: `bleducmills@gitlab.com`

✨ GitLab profile page: https://gitlab.com/leducmills
